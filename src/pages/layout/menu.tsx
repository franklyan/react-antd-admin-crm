import React, { FC, useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Menu } from 'antd'

import { MenuList } from '../../interface/layout/menu.interface'
import { CustomIcon } from './customIcon'
import { AppState } from '~/stores'
import { addTag } from '~/actions/tagsView.action'

const { SubMenu, Item } = Menu

interface Props {
  menuList: MenuList
}

const MenuComponent: FC<Props> = ({ menuList }) => {
  const [openKeys, setOpenkeys] = useState<string[]>([])
  const [selectedKeys, setSelectedKeys] = useState<string[]>([])
  const { collapsed } = useSelector((state: AppState) => state.globalReducer)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { pathname } = useLocation()

  const getTitie = (menu: MenuList[0]) => {
    return (
      <span style={{ display: 'flex', alignItems: 'center' }}>
        <CustomIcon type={menu.icon!} />
        <span>{menu.label}</span>
      </span>
    )
  }

  const onMenuClick = (menu: MenuList[0]) => {
    if (menu.path === pathname) return
    const { key, label, path } = menu
    setSelectedKeys([key])
    dispatch(
      addTag({
        id: key,
        label,
        path,
        closable: true
      })
    )
    navigate(path)
  }

  useEffect(() => {
    setSelectedKeys([pathname])
    setOpenkeys(collapsed ? [] : ['/' + pathname.split('/')[1]])
  }, [collapsed, pathname])

  return (
    <Menu
      mode="inline"
      theme="light"
      selectedKeys={selectedKeys}
      openKeys={openKeys}
      onOpenChange={keys => {
        const items = (keys as Array<any>).pop()
        setOpenkeys(items)
      }}
    >
      {menuList?.map(menu =>
        menu.children ? (
          <SubMenu key={menu.path} title={getTitie(menu)}>
            {menu.children.map(child => (
              <Item key={child.path} onClick={() => onMenuClick(child)}>
                {child.label}
              </Item>
            ))}
          </SubMenu>
        ) : (
          <Item key={menu.path} onClick={() => onMenuClick(menu)}>
            {getTitie(menu)}
          </Item>
        )
      )}
    </Menu>
  )
}

export default MenuComponent
