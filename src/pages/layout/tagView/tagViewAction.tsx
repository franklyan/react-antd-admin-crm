import React, { FC } from 'react'
import { Menu, Dropdown } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { SettingOutlined } from '@ant-design/icons'

import { removeTag, removeOtherTag, removeAllTag } from '~/actions/tagsView.action'
import { AppState } from '~/stores'

const TagsViewAction: FC = () => {
  const { activeTagId } = useSelector((state: AppState) => state.tagsViewlReducer)
  const dispatch = useDispatch()
  return (
    <Dropdown
      overlay={
        <Menu>
          <Menu.Item key="0" onClick={() => dispatch(removeTag(activeTagId))}>
            closeCurrent
          </Menu.Item>
          <Menu.Item key="1" onClick={() => dispatch(removeOtherTag())}>
            closeOther
          </Menu.Item>
          <Menu.Item key="2" onClick={() => dispatch(removeAllTag())}>
            closeAll
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="3" onClick={() => dispatch(removeAllTag())}>
            dashboard
          </Menu.Item>
        </Menu>
      }
    >
      <span id="pageTabs-actions">
        <SettingOutlined className="tagsView-extra" />
      </span>
    </Dropdown>
  )
}

export default TagsViewAction
