import React, { FC, useEffect, Suspense } from 'react'
import { Layout } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import { Outlet, useLocation, useNavigate } from 'react-router'

import { AppState } from '~/stores'
import { setGlobalItem } from '~/actions/global.action'

import MenuComponent from './menu'
import { menuList } from '~/config/menus'
import HeaderComponent from './header'
import TagsView from './tagView'
import SuspendFallbackLoading from './suspendFallbackLoading'

import { LayoutContainer } from './style'

const { Sider, Content } = Layout
const WIDTH = 992

const LayoutPage: FC = () => {
  const { collapsed } = useSelector((state: AppState) => state.globalReducer)
  const dispatch = useDispatch()
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    if (location.pathname === '/') {
      navigate('/dashboard')
    }
  }, [navigate, location])

  const toggle = () => {
    dispatch(
      setGlobalItem({
        collapsed: !collapsed
      })
    )
  }

  useEffect(() => {
    window.onresize = () => {
      const rect = document.body.getBoundingClientRect()
      const needCollapse = rect.width < WIDTH
      dispatch(
        setGlobalItem({
          collapsed: needCollapse
        })
      )
    }
  }, [dispatch])

  return (
    <LayoutContainer>
      <HeaderComponent collapsed={collapsed} toggle={toggle} />
      <Layout>
        <Sider className="layout-page-sider" trigger={null} collapsible collapsed={collapsed} breakpoint="md">
          <MenuComponent menuList={menuList} />
        </Sider>
        <Content className="layout-page-content">
          <TagsView />
          <Suspense fallback={<SuspendFallbackLoading />}>
            <Outlet />
          </Suspense>
        </Content>
      </Layout>
    </LayoutContainer>
  )
}

export default LayoutPage
