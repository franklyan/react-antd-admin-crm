import React, { FC, useState, useEffect, useCallback } from 'react'
import { Modal, Tree, Spin } from 'antd'
import { useSelector } from 'react-redux'
import { TreeNodeNormal } from 'antd/lib/tree/Tree'

import { Role } from '~/interface/permission/role.interface'
import { getMenuList } from '~/api/layout.api'
import { AppState } from '~/stores'
import usePrevious from '~/hooks/usePrevious'

interface Values extends Role {}

interface RoleModifyDialogProps {
  values: Values
  visible: boolean
  onAuthorize: (values: string[]) => void
  onCancel: () => void
}

const RoleAuthorizeDialog: FC<RoleModifyDialogProps> = ({ onAuthorize, onCancel, visible, values }) => {
  const { menuList } = useSelector((state: AppState) => state.userReducer)
  const [checkedKeys, setCheckedKeys] = useState<string[]>([])
  const [treeData, setTreeData] = useState<TreeNodeNormal[]>([])
  const prevRoleId = usePrevious(values.id)

  const onSubmit = async () => {
    onAuthorize(checkedKeys)
  }

  const initData = useCallback(async () => {
    const { result, status } = await getMenuList()
    // console.log('result: ', result)
    if (status) {
      // format
      setTreeData(
        result.map(a => ({
          title: a.label,
          key: a.key,
          children: a.children?.map(b => ({
            title: b.label,
            key: b.key
          }))
        }))
      )
    }
  }, [])

  // Set the checkedKeys when the user menu list is loaded
  useEffect(() => {
    if (menuList.length) {
      setCheckedKeys(menuList.map(m => m.key))
    }
  }, [menuList])

  useEffect(() => {
    // Optimize: Opening a dialog repeatedly will not trigger initData method. #usePrevious hooks
    // Locale changed will trigger initData in any case.
    if (visible && prevRoleId !== values.id) {
      console.log('initData')
    }
  }, [initData, visible, prevRoleId, values.id])

  return (
    <Modal title={'authorize'} visible={visible} onOk={onSubmit} onCancel={onCancel}>
      {treeData.length ? (
        <Tree
          checkable
          defaultExpandAll
          checkedKeys={checkedKeys}
          onCheck={keys => setCheckedKeys(keys as string[])}
          treeData={treeData}
        />
      ) : (
        <Spin />
      )}
    </Modal>
  )
}

export default RoleAuthorizeDialog
