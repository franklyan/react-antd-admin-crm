import React, { FC } from 'react'
import useGetRoleFormItem from './useGetRoleForm'
import { Button } from 'antd'

const RoleSearch: FC = () => {
  const { Form, form, Name, Code, Status } = useGetRoleFormItem({ name: 'searchForm', responsive: true })

  const onSearch = () => {
    //
  }

  return (
    <Form>
      <Name />
      <Code />
      <Status />
      <Form.Item>
        <Button type="primary" onClick={onSearch}>
          search
        </Button>
        <Button onClick={() => form.resetFields()}>reset</Button>
      </Form.Item>
    </Form>
  )
}

export default RoleSearch
