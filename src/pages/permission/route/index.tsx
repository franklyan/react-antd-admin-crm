import React, { FC } from 'react'

import { RouterContainer } from './style'

const RoutePermissionPage: FC = () => {
  return (
    <RouterContainer>
      <p className="permission-intro">loginResult</p>
    </RouterContainer>
  )
}

export default RoutePermissionPage
