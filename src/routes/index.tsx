import React, { lazy, FC } from 'react'
import { useRoutes } from 'react-router-dom'

import Dashboard from '~/pages/dashboard'
import LoginPage from '~/pages/login'
import LayoutPage from '~/pages/layout'
import { PartialRouteObject } from 'react-router'
import WrapperRouteComponent from './config'

const NotFound = lazy(() => import(/* webpackChunkName: "404'"*/ '~/pages/404'))
const Documentation = lazy(() => import(/* webpackChunkName: "404'"*/ '~/pages/doucumentation'))
const RoutePermission = lazy(() => import(/* webpackChunkName: "route-permission"*/ '~/pages/permission/route'))
const PermissionConfig = lazy(() => import(/* webpackChunkName: "PermissionConfig"*/ '~/pages/permission/config'))
const AccountPage = lazy(() => import(/* webpackChunkName: "account'"*/ '~/pages/account'))

const routeList: PartialRouteObject[] = [
  {
    path: 'login',
    element: <WrapperRouteComponent element={<LoginPage />} titleId="title.login" />
  },
  {
    path: '',
    element: <WrapperRouteComponent element={<LayoutPage />} titleId="" />,
    children: [
      {
        path: 'dashboard',
        element: <WrapperRouteComponent element={<Dashboard />} titleId="title.dashboard" />
      },
      {
        path: 'documentation',
        element: <WrapperRouteComponent element={<Documentation />} titleId="title.documentation" />
      },
      {
        path: 'permission/route',
        element: <WrapperRouteComponent element={<RoutePermission />} titleId="title.permission.route" auth />
      },
      {
        path: 'permission/config',
        element: <WrapperRouteComponent element={<PermissionConfig />} titleId="title.permission.config" />
      },
      {
        path: 'account',
        element: <WrapperRouteComponent element={<AccountPage />} titleId="title.account" />
      },
      {
        path: '*',
        element: <WrapperRouteComponent element={<NotFound />} titleId="title.notFount" />
      }
    ]
  }
]

const RenderRouter: FC = () => {
  const element = useRoutes(routeList)
  return element
}

export default RenderRouter
