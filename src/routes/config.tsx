import React, { FC } from 'react'
import { Route } from 'react-router-dom'
import { RouteProps } from 'react-router'
import PrivateRoute from './pravateRoute'

export interface WrapperRouteProps extends RouteProps {
  titleId: string
  auth?: boolean
}

const WrapperRouteComponent: FC<WrapperRouteProps> = ({ titleId, auth, ...props }) => {
  const WithRoute = auth ? PrivateRoute : Route
  if (titleId) {
    document.title = titleId
  }
  return <WithRoute {...props} />
}

export default WrapperRouteComponent
