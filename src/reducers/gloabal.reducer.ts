import { GlobalState, GlobalActions } from '~/actions/global.action'

const globalState: GlobalState = {
  collapsed: false,
  noticeCount: 0,
  newUser: JSON.parse(localStorage.getItem('newUser')!) ?? true
}

export const globalReducer = (state = globalState, actions: GlobalActions): GlobalState => {
  switch (actions.type) {
    case 'SETGLOBALITEM':
      return { ...state, ...actions.payload }
    default:
      return state
  }
}
