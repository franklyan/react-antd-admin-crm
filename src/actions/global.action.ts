import { Action } from 'redux'

export interface GlobalState {
  /** menu collapsed status */
  collapsed: boolean

  /** notification count */
  noticeCount: number

  /** Is first time to view the site ? */
  newUser: boolean
}

const SETGLOBALITEM = 'SETGLOBALITEM'

type SETGLOBALITEM = typeof SETGLOBALITEM

interface SetGloabalItem extends Action<SETGLOBALITEM> {
  payload: Partial<GlobalState>
}

export const setGlobalItem = (payload: Partial<GlobalState>): SetGloabalItem => ({
  type: 'SETGLOBALITEM',
  payload
})

export type GlobalActions = SetGloabalItem
