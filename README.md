# only

## 技术栈

- 打包相关：
  - [webpack^4.43.0](https://webpack.js.org/)
  - [yarn](https://yarnpkg.com/lang/zh-Hans/)
- 应用：
  - [typescript^3.2.4](https://www.typescriptlang.org/)
  - [react^16.13.x](https://reactjs.org/)
  - [antd^4.x](https://ant.design/)
  - [styled-components](https://styled-components.com/)
- 规范约束
  - [husky](https://github.com/typicode/husky)
  - [cz-conventional-changelog](https://github.com/typicode/husky)
  - prettier
  - eslint

## 开发及发布

### 快速开始

```bash
yarn

# dev mode
yarn start

# prod
yarn build
```
